const express = require('express');
const controller = require('../controller/users.controller');

const router = express.Router();

router.get('/', controller.index);

router.get('/create', controller.create);

router.get('/search', controller.search);

router.get('/:id', controller.view);

router.post('/create', controller.pCreate);


module.exports = router;