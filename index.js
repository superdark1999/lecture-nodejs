const express = require('express');
const bodyParser = require('body-parser');
const userRouter = require('./routes/users.routes');

const app = express();
const port = 3000;

app.set('view engine', 'pug');
app.set('views', './views');


app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.use(express.static('public'));

app.use('/users', userRouter);

app.listen(port, function() {
    console.log('server listen on port' + port);
})

